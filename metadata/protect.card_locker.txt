Categories:Money
License:GPLv3+
Web Site:
Source Code:https://github.com/brarcher/loyalty-card-locker
Issue Tracker:https://github.com/brarcher/loyalty-card-locker/issues

Auto Name:Loyalty Card Locker
Summary:Saves all your loyalty cards on your phone
Description:
Saves all of your store loyalty cards on your phone, removing the need to carry
them around.
.

Repo Type:git
Repo:https://github.com/brarcher/loyalty-card-locker

Build:0.1,1
    commit=v0.1
    subdir=app
    gradle=yes

Build:0.2,2
    commit=v0.2
    subdir=app
    gradle=yes

Build:0.3,3
    commit=v0.3
    subdir=app
    gradle=yes

Build:0.4,4
    commit=v0.4
    subdir=app
    gradle=yes

Build:0.5,5
    commit=v0.5
    subdir=app
    gradle=yes

Build:0.6,6
    commit=v0.6
    subdir=app
    gradle=yes

Build:0.7,7
    commit=v0.7
    subdir=app
    gradle=yes

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:0.7
Current Version Code:7
