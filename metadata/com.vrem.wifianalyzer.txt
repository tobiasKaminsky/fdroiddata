Categories:Connectivity
License:Apache2
Web Site:https://github.com/VREMSoftwareDevelopment/WifiAnalyzer/blob/HEAD/README.md
Source Code:https://github.com/VREMSoftwareDevelopment/WifiAnalyzer
Issue Tracker:https://github.com/VREMSoftwareDevelopment/WifiAnalyzer/issues

Auto Name:WiFi Analyzer
Summary:Optimize your WiFi network
Description:
Optimize your WiFi network by examining surrounding WiFi networks, measuring
their signal strength as well as identifying crowded channels.

Features:

* Identify nearby Access Points
* Graph channels signal strength
* Graph Access Point signal strength over time
* Analyze WiFi networks to rate channels
.

Repo Type:git
Repo:https://github.com/VREMSoftwareDevelopment/WifiAnalyzer.git

Build:1.4.2.33,9
    commit=V1.4.1.31
    subdir=app
    gradle=yes

Build:1.5.3,11
    commit=V1.5.3-F-DROID
    subdir=app
    gradle=yes

Build:1.5.4,12
    commit=V1.5.4-F-DROID
    subdir=app
    gradle=yes

Build:1.5.5,13
    commit=V1.5.5-F-DROID
    subdir=app
    gradle=yes

Build:1.5.6,14
    commit=V1.5.6-F-DROID
    subdir=app
    gradle=yes

Build:1.5.7,15
    commit=V1.5.7-F-DROID
    subdir=app
    gradle=yes

Maintainer Notes:
Upstream generates the version information at compile time.
.

Auto Update Mode:None
Update Check Mode:None
Current Version:1.5.7
Current Version Code:15
