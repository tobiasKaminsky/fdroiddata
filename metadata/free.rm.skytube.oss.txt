AntiFeatures:NonFreeNet
Categories:Multimedia,Internet
License:GPLv3
Web Site:https://ram-on.github.io/SkyTube
Source Code:https://github.com/ram-on/SkyTube
Issue Tracker:https://github.com/ram-on/SkyTube/issues
Changelog:https://github.com/ram-on/SkyTube/releases

Auto Name:SkyTube
Summary:A simple and feature-rich YouTube player
Description:
SkyTube is a YouTube player that allows you to:

* explore Trending and Most Popular videos,
* browse YouTube channels,
* play YouTube videos,
* view video comments,
* search videos, music and channels
* channel subscription

... all at the tip of your fingers.

More features will be added in the near future.
.

Repo Type:git
Repo:https://github.com/ram-on/SkyTube

Build:1.0 OSS,1
    commit=v1.0
    subdir=app
    gradle=oss

Build:2.0,2
    commit=v2.0
    subdir=app
    gradle=oss

Maintainer Notes:
* Current versions include app/libs/*.jar. Remove them.
* No AUM applies, since version != tag.
.

Auto Update Mode:Version v%v
Update Check Mode:Tags
Current Version:2.0
Current Version Code:2
